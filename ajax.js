$(document).ready(function () {
    var startDate;
    var endDate;

    $('#date_picker1').datepicker({
        dateFormat: "dd-mm-yy",
    });

    $('#date_picker2').datepicker({
        dateFormat: 'dd-mm-yy'
    });


    $('#date_picker1').change(function () {
        startDate = $(this).datepicker('getDate');

        $('#date_picker2').datepicker('option', 'minDate', startDate);
    });


    $('#date_picker2').change(function () {
        endDate = $(this).datepicker('getDate');

        $('#date_picker1').datepicker('option', 'maxDate', endDate);
    });
});




document.getElementById('go').onclick = function () {
    document.getElementById("result-checkin").value = document.getElementById("date_picker1").value;
    document.getElementById("result-checkout").value = document.getElementById("date_picker2").value;
    document.getElementById("result-adult").value = document.getElementById("adult").value;
    document.getElementById("result-room").value = document.getElementById("rooms").value;
    document.getElementById("result-children").value = document.getElementById("children").value;
}

