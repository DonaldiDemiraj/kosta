<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Entalgo Apartaments</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="main.css">

    <!-- jquery datapicker -->
    <link rel="stylesheet" type="text/css" href="jquery-ui.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
    <!-- end -->



    <script src="https://kit.fontawesome.com/dbed6b6114.js" crossorigin="anonymous"></script>
    <link rel="icon" href="images/entalgo.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">

    <!-- Bootstrap 4 CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!--Bootstrap Datepicker CSS-->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">


</head>

<body>
    <!-- header -->
    <header class="header" id="header">

        <div class="head-top">
            <div class="site-name">
                <img src="images/entalgo.png" alt="" style="width: 200px;">
                <span>ENTALGO-APARTAMENTS</span>
            </div>
            <div class="site-nav">
                <span id="nav-btn">MENU <i class="fas fa-bars"></i></span>
            </div>
        </div>

        <div class="head-bottom flex">
            <h2>Modern Apartment Korça</h2>
        </div>
    </header>
    <!-- end of header -->

    <!-- side navbar -->
    <div class="sidenav" id="sidenav">
        <span class="cancel-btn" id="cancel-btn">
            <i class="fas fa-times"></i>
        </span>

        <ul class="navbar">
            <li><a href="#header">home</a></li>
            <li><a href="#services">services</a></li>
            <li><a href="#apartaments">apartaments</a></li>
            <li><a href="#customers">customers</a></li>
        </ul>
        <img src="images/WhatsApp Image 2021-11-26 at 17.04.13.jpeg" alt="">
    </div>
    <!-- end of side navbar -->

    <!-- fullscreen modal -->
    <div id="modal"></div>
    <!-- end of fullscreen modal -->

    <!-- body content  -->
    <section class="services sec-width" id="services">
        <div class="title">
            <h2>House Rules</h2>
        </div>
        <div class="services-container">
            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-door-closed"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>Guests are required to handover their keys</h2>
                    <p>Guests are given the keys upon arrival at the apartament and are kindly asked to lock the
                        apartment when leaving. Guests are required to handover their keys to the agent upon departure.
                        If guests lose or do not return the keys they are obligated to pay the penalty of 5 EUR.</p>
                </div>
            </article>
            <!-- end of single service -->

            <!-- single service -->

            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-user-check"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>CheckIn & CheckOut</h2>
                    <p>Check in is from 15:00 until 20:00. Check out is from 08:00 until 11:00.
                    </p>

                </div>
            </article>
            <!-- end of single service -->

            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-hand-peace"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>Not cause noise that can disturb the neighbours.</h2>
                    <p>From 23:00 to 07:00 hours is the time of night peace.We invite every guest to be careful
                        during
                        this period and not cause noise that can disturb the neighbours. </p>

                </div>
            </article>
            <!-- end of single service -->

            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-user"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>Valid identity</h2>
                    <p>Apartament can only be used by guests who provide a valid identity document upon check in.</p>

                </div>
            </article>
            <!-- end of single service -->
            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-money-bill"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>Cash Payments.</h2>
                    <p>This property only accepts cash payments after the arrival of the Guest based on the booking
                        details. </p>

                </div>
            </article>
            <!-- end of single service -->
            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-fast-forward"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>Check out earlier.</h2>
                    <p>If during your stay, for whatever reason, you need to check out earlier, it is necessary to
                        inform the agent at least the day before departure. </p>

                </div>
            </article>
            <!-- end of single service -->
            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-house-damage"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>House Damage</h2>
                    <p>For any damage caused by the Guest to the property, the Guest is due to cover the cost of damage
                        to the apartment. </p>

                </div>
            </article>
            <!-- end of single service -->
            <!-- single service -->
            <article class="service">
                <div class="service-icon">
                    <span>
                        <i class="fas fa-building"></i>
                    </span>
                </div>
                <div class="service-content">
                    <h2>It is not allowed to take out from the apartament inventory.</h2>
                    <p>It is not allowed to take out from the apartament inventory (pillows, blankets, towels, electric
                        appliances etc.). </p>
                </div>
            </article>
            <!-- end of single service -->
        </div>
        <article class="services" style="text-align: center;">
            If you have any suggestion on apartment service or if you have any technical problem in your room,
            please feel free to inform us at your earliest convenience. In case of late notice for any complaints
            that you might have, when our agent has no chance to react, those situations will not be considered as a
            reason for accommodation rate discount.
        </article>
    </section>

    <div class="book" id="booked">
        <div id="modern-apartament-korca-2">
            <div id="modern-apartament-korca-3">
                <form class="book-form" method="get">
                    <div class="form-item">
                        <label for="apartaments">Select The Apartaments : </label>
                        <select name="apartament" id="chekout-date" class="apartaments"
                            onchange="populate(this.id,'result-apartament')">
                            <option value="">Select One</option>
                            <option value="1">Modern Apartament Korca</option>
                            <option value="2">Modern Apartament Korca 2</option>
                            <option value="3">Modern Apartament Korca 3</option>
                        </select>
                    </div>
                    <div class="form-item">
                        <label for="checkin-date">Check In Date: </label>
                        <input type="text" name="checkinFiel" id="date_picker1" placeholder="mm/dd/yyyy">
                        <!-- <input name="checkinFiel" type="text" id="demo" placeholder="mm/dd/yyyy"> -->
                    </div>

                    <div class="form-item">
                        <label for="checkout-date">Check Out Date: </label>
                        <input type="text" name="checkoutFiel" id="date_picker2" placeholder="mm/dd/yyyy"
                            max="2022-01-20">
                        <!-- <input name="checkoutFiel" type="date" id="txtDate" max="2022-01-20"> -->
                    </div>
                    <div class="form-item">
                        <label for="adult">Adults: </label>
                        <input name="adult" type="number" min="1" value="1" id="adult" class="input-a">
                    </div>
                    <div class="form-item">
                        <label for="children">Children: </label>
                        <input name="childre" type="number" min="1" value="1" id="children" class="input-c">
                    </div>
                    <div class="form-item">
                        <label for="rooms">Rooms: </label>
                        <input name="room" type="number" min="1" value="1" id="rooms" class="input-r">
                    </div>
                    <div class="form-item">
                        <input type="button" class="btn btn-primary send" data-toggle="modal" id="go"
                            data-target="#ModalLoginForm" id="subm" value="Book Now"
                            style="height: 50px; margin-bottom: 15px;">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal HTML Markup -->
    <div id="ModalLoginForm" class="modal fade" id="edit">
        <div class="modal-dialog wrapper" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title">Booking Email</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>

                <div id="wrapper" class="modal-body">
                    <form class="form" action="index.html" method="post">
                        <div>
                            <label for="apartaments">Select The Apartaments : </label><br>
                            <select name="apartaments" id="result-apartament" class="form-control input-lg"
                                style="border: 1px solid;" name="apartaments">
                                <option value="">Select One</option>
                                <option value="1">Modern Apartament Korca</option>
                                <option value="2">Modern Apartament Korca 2</option>
                                <option value="3">Modern Apartament Korca 3</option>
                            </select>
                        </div>
                        <div class="form-item">
                            <label for="checkin-date" id="result-checkin">Check In Date: </label>
                            <div><input type="date" class="form-control input-lg" id="result-checkin"
                                    placeholder="mm/dd/yyyy" name="checkinField">
                            </div>
                        </div>

                        <div class="form-item">
                            <label for="checkout-date" id="checkoutField">Check Out Date: </label>
                            <input type="date" id="result-checkout" class="form-control input-lg" max="2022-01-20"
                                name="checkoutField">
                        </div>
                        <div class="form-item">
                            <label for="adult" id="adults">Adults: </label>
                            <input type="number" class="form-control input-lg" min="1" value="1" id="result-adult"
                                name="adults">
                        </div>
                        <div class="form-item">
                            <label for="children" id="children">Children: </label>
                            <input type="number" class="form-control input-lg" min="1" value="1" id="result-children"
                                name="children">
                        </div>
                        <div class="form-item">
                            <label for="rooms" id="rooms">Rooms: </label>
                            <input type="number" class="form-control input-lg" min="1" value="1" id="result-room"
                                name="rooms">
                        </div>

                        <br>
                        <h1>Email</h1>

                        <input type="hidden" name="_token" value="">
                        <div class="form-group">
                            <label class="control-label" id="email">E-Mail Address</label>
                            <div>
                                <input type="email" class="form-control input-lg" name="email" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" id="cel">Nr Cel</label>
                            <div>
                                <input type="number" class="form-control input-lg" name="cel">
                            </div>
                        </div>
                        <div class="form-item">
                            <input type="submit" class="btn btn-primary" data-toggle="modal"
                                data-target="#ModalLoginForm" id="subm" value="Book Now"
                                style="height: 50px; margin-bottom: 15px;">
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <section class="rooms sec-width" id="apartaments">
        <div class="title">
            <h2>Apartaments</h2>
        </div>
        <div class="rooms-container">
            <!-- single room -->
            <article class="room">
                <div class="room-image">
                    <img src="images/1.jpg" alt="room image">
                </div>
                <div class="room-text">
                    <h3>Modern Apartment Korca</h3>
                    <ul>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Non-smoking rooms
                        </li>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Free parking
                        </li>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Free WiFi
                        </li>
                    </ul>
                    <p>COMING SOON: Entire Modern Apartment is located in Korçë and offers a bar. The air-conditioned
                        accommodation is 43 km from Kastoria.

                        The apartment is equipped with 1 bedroom, a kitchen with a microwave and a fridge, a washing
                        machine, and 1 bathroom with bathrobes and slippers. A flat-screen TV is provided.

                        Guests at COMING SOON: Entire Modern Apartment can enjoy hiking nearby, or make the most of the
                        garden.

                        We speak your language!</p>
                    <div class="link">
                        <div class="form-item">
                            <a href="#booked" style="text-decoration: none;">
                                <input type="submit" id="update" class="btn" value="BookNow" style="background: white">
                            </a>
                        </div>
                        <button class="btn" style="background: white">
                            <a href="https://www.airbnb.com/h/modern-ap-1" target="_blank">
                                <img style="width: 5rem;" src="./images/airbnb-logo-10.png">
                            </a>
                        </button>
                        <button class="btn" style="background: white">
                            <a target="_blank"
                                href="https://www.booking.com/hotel/al/coming-soon-entire-modern-apartment.en-gb.html?aid=397594;label=gog235jc-1FCAEoggI46AdIM1gDaAaIAQGYAQm4ARfIAQzYAQHoAQH4AQyIAgGoAgO4AoiIg40GwAIB0gIkNDMzZDJjYzctYTA4Mi00MGNjLWJjOTgtMjZkM2ZjNmI4MWQx2AIG4AIB;sid=61a8a94f02f64127e5ea59300a65c088;all_sr_blocks=735072901_338281804_2_0_0;checkin=2021-12-09;checkout=2021-12-10;dest_id=-105824;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=57;highlighted_blocks=735072901_338281804_2_0_0;hpos=7;matching_block_id=735072901_338281804_2_0_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=price;sr_pri_blocks=735072901_338281804_2_0_0__3960;srepoch=1637935169;srpvid=2d56624e9d39007f;type=total;ucfs=1&#hotelTmpl">
                                <img src="./images/booking.png">
                            </a>
                        </button>
                    </div>
                </div>
            </article>
            <!-- end of single room -->
            <!-- single room -->
            <article class="room">
                <div class="room-image">
                    <img src="images/2.jpeg" alt="room image">
                </div>
                <div class="room-text">
                    <h3>Modern Apartment Korca 2</h3>
                    <ul>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Non-smoking rooms
                        </li>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Free parking
                        </li>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Free WiFi
                        </li>
                    </ul>
                    <p>Modern Apartment Korca 2 is situated in Korçë. Guests staying at this apartment have access to a
                        balcony.

                        The apartment comes with a flat-screen TV. The air-conditioned accommodation is equipped with a
                        kitchen.

                        Kastoria is 43 km from the apartment, while Pogradec is 32 km away.

                        We speak your language!</p>

                    <div class="link">

                        <div class="form-item">
                            <a href="#booked" style="text-decoration: none;">
                                <input type="submit" id="update2" class="btn" value="BookNow" style="background: white">
                            </a>
                        </div>

                        <button class="btn" style="background: white">
                            <a href="https://www.airbnb.com/h/modern-ap-2" target="_blank">
                                <img style="width: 5rem;" src="./images/airbnb-logo-10.png">
                            </a>
                        </button>
                        <button class="btn" style="background: white">
                            <a target="_blank"
                                href="https://www.booking.com/hotel/al/modern-apartment-korca-2.en-gb.html?aid=397594;label=gog235jc-1FCAEoggI46AdIM1gDaAaIAQGYAQm4ARfIAQzYAQHoAQH4AQyIAgGoAgO4AoiIg40GwAIB0gIkNDMzZDJjYzctYTA4Mi00MGNjLWJjOTgtMjZkM2ZjNmI4MWQx2AIG4AIB;sid=61a8a94f02f64127e5ea59300a65c088;all_sr_blocks=792543601_339036748_2_0_0;checkin=2021-12-09;checkout=2021-12-10;dest_id=-105824;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=25;highlighted_blocks=792543601_339036748_2_0_0;hpos=0;matching_block_id=792543601_339036748_2_0_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=price;sr_pri_blocks=792543601_339036748_2_0_0__3135;srepoch=1637935151;srpvid=2d56624e9d39007f;type=total;ucfs=1&#hotelTmpl">
                                <img src="./images/booking1.png">
                            </a>
                        </button>
                    </div>
                </div>
            </article>
            <!-- end of single room -->
            <!-- single room -->


            <article class="room">
                <div class="room-image">
                    <img src="images/3.jpeg" alt="room image">
                </div>
                <div class="room-text">
                    <h3>Modern Apartment Korca 3</h3>
                    <ul>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Non-smoking rooms
                        </li>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Free parking
                        </li>
                        <li>
                            <i class="fas fa-arrow-alt-circle-right"></i>
                            Free WiFi
                        </li>
                    </ul>
                    <p>Modern Apartment Korca 3 is located in Korçë. Guests staying at this apartment have access to a
                        fully equipped kitchen.

                        The apartment is fitted with a flat-screen TV and 1 bedroom.

                        Kastoria is 43 km from the apartment.

                        We speak your language!</p>
                    <div class="link">

                        <div class="form-item">
                            <a href="#booked" style="text-decoration: none;">
                                <input type="submit" id="update3" class="btn" value="BookNow" style="background: white">
                            </a>
                        </div>

                        <button class="btn" style="background: white">
                            <a href="https://www.airbnb.com/h/modern-ap-3" target="_blank">
                                <img style="width: 5rem;" src="./images/airbnb-logo-10.png">
                            </a>
                        </button>
                        <button class="btn" style="background: white">
                            <a target="_blank"
                                href="https://www.booking.com/hotel/al/modern-apartment-korca-3.en-gb.html?aid=397594;label=gog235jc-1FCAEoggI46AdIM1gDaAaIAQGYAQm4ARfIAQzYAQHoAQH4AQyIAgGoAgO4AoiIg40GwAIB0gIkNDMzZDJjYzctYTA4Mi00MGNjLWJjOTgtMjZkM2ZjNmI4MWQx2AIG4AIB;sid=61a8a94f02f64127e5ea59300a65c088;all_sr_blocks=792546201_339036838_2_0_0;checkin=2021-12-09;checkout=2021-12-10;dest_id=-105824;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=33;highlighted_blocks=792546201_339036838_2_0_0;hpos=8;matching_block_id=792546201_339036838_2_0_0;no_rooms=1;req_adults=2;req_children=0;room1=A%2CA;sb_price_type=total;sr_order=price;sr_pri_blocks=792546201_339036838_2_0_0__3300;srepoch=1637935151;srpvid=2d56624e9d39007f;type=total;ucfs=1&#hotelTmpl">
                                <img src="./images/booking1.png">
                            </a>
                        </button>
                    </div>
                </div>
            </article>
            <!-- end of single room -->
        </div>
    </section>




    <div id="google-reviews"></div>

    <section id="serverResponse" class="review-section">
    </section>





    <section class="customers" id="customers">

        <div class="sec-width">

            <div class="title">
                <h2>customers</h2>
            </div>

            <div class="customers-container">
                <!-- single customer -->
                <div class="customer">
                    <div class="rating">
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="far fa-star"></i></span>
                    </div>
                    <h3>We Loved it</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat beatae veritatis provident
                        eveniet praesentium veniam cum iusto distinctio esse, vero est autem, eius optio
                        cupiditate?</p>

                    <img src="images/cus1.jpg" alt="customer image">
                    <span>Customer Name, Country</span>
                </div>
                <!-- end of single customer -->
                <!-- single customer -->
                <div class="customer">
                    <div class="rating">
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="far fa-star"></i></span>
                    </div>
                    <h3>Comfortable Living</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat beatae veritatis provident
                        eveniet praesentium veniam cum iusto distinctio esse, vero est autem, eius optio cupiditate?</p>
                    <img src="images/cus2.jpg" alt="customer image">
                    <span>Customer Name, Country</span>
                </div>
                <!-- end of single customer -->
                <!-- single customer -->
                <div class="customer">
                    <div class="rating">
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="fas fa-star"></i></span>
                        <span><i class="far fa-star"></i></span>
                    </div>
                    <h3>Nice Place</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat beatae veritatis provident
                        eveniet praesentium veniam cum iusto distinctio esse, vero est autem, eius optio cupiditate?</p>
                    <img src="images/cus3.jpg" alt="customer image">
                    <span>Customer Name, Country</span>
                </div>
                <!-- end of single customer -->
            </div>
        </div>
    </section>
    <!-- end of body content -->

    <!-- footer -->
    <footer class="footer">
        <div class="footer-container">
            <div>
                <h2>Social Media</h2>
                <ul class="social-icons">
                    <!-- <li class="flex">
                        <i class="fa fa-twitter fa-2x"></i>
                    </li> -->
                    <li class="flex">
                        <a target="_blank" href="https://www.instagram.com/modern_apartments_korce/">
                            <i class="fa fa-instagram fa-2x"></i></a>
                    </li>
                    <li class="flex">
                        <a target="_blank" href="https://www.facebook.com/modern.apartment.korce/?__tn__=-UC*F">
                            <i class="fa fa-facebook fa-2x"></i>
                        </a>
                    </li>
                    <li class="flex">
                        <a target="_blank" href="https://web.whatsapp.com/send?phone=+355694335312">
                            <i class="fa fa-whatsapp fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div>
                <h2>Locations</h2>
                <div style="width: 100%">
                    <iframe width="100%" height="200" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                        src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=modern%20apartament%20korca-1+(My%20Business%20Name)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"><a
                            href="https://www.gps.ie/car-satnav-gps/">sat navs</a>
                    </iframe>
                </div>
            </div>



            <div>
                <h2>Useful Links</h2>
                <div class="contact-item">
                    <span>
                        <i class="fas fa-map-marker-alt"></i>
                    </span>
                    <span>
                        Rr, Rruga Midhi Kostani, KORCE ALBANIA
                    </span>
                </div>
                <div class="contact-item">
                    <span>
                        <i class="fas fa-phone-alt"></i>
                    </span>
                    <span>
                        <a href="tel:067 727 6751">067 727 6751</a>
                    </span>
                </div>
                <div class="contact-item">
                    <span>
                        <i class="fas fa-envelope"></i>
                    </span>
                    <span>
                        <a href="mailto:apartaments@entalgo.com">apartaments@entalgo.com</a>
                    </span>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js"></script>

    <script src="script.js"></script>
    <script src="result.js"></script>
    <script src="ajax.js"></script>
    <script type="text/javascript" src="jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="jquery-ui.min.js"></script>


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Bootstrap 4 JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Bootstrap Datpicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


    <script>
    var datesForDisable = ["2021-12-24", "2022-01-10"]

    $('#date_picker1').datepicker({
        format: 'yy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        maxDate: '+1m +10d',
        minDate: 0
    });
    </script>


</body>

</html>