//get data from first dropdown apartaments and past in dropdown modal
function populate(s1, s2) {
    var s1 = document.getElementById(s1);
    var s2 = document.getElementById(s2);

    s2.innerHTML = "";


    if (s1.value == "1") {
        var optionArray = ['moder apartament korca|Modern Apartament Korca'];
    } else if (s1.value == "2") {
        var optionArray = ['moder apartament korca 2|Modern Apartament Korca 2'];
    } else if (s1.value == "3") {
        var optionArray = ['moder apartament korca 3|Modern Apartament Korca 3'];
    }

    for (var option in optionArray) {
        var pair = optionArray[option].split("|");
        var newoption = document.createElement("option");

        newoption.value = pair[0];
        newoption.innerHTML = pair[1];
        s2.options.add(newoption);
    }
}
