const navBtn = document.getElementById('nav-btn');
const cancelBtn = document.getElementById('cancel-btn');
const sideNav = document.getElementById('sidenav');
const modal = document.getElementById('modal');

navBtn.addEventListener("click", function () {
    sideNav.classList.add('show');
    modal.classList.add('showModal');
});

cancelBtn.addEventListener('click', function () {
    sideNav.classList.remove('show');
    modal.classList.remove('showModal');
});

window.addEventListener('click', function (event) {
    if (event.target === modal) {
        sideNav.classList.remove('show');
        modal.classList.remove('showModal');
    }
});


//booking button bor value in appartaments
$(document).ready(function () {
    $('#update').click(function () {
        $("#chekout-date").val('1');
    })
});

$(document).ready(function () {
    $('#update2').click(function () {
        $("#chekout-date").val('2');
    })
});

$(document).ready(function () {
    $('#update3').click(function () {
        $("#chekout-date").val('3');
    })
});




//valid data for future
// $(function () {
//     var dtToday = new Date();
//     var month = dtToday.getMonth() + 1;
//     var day = dtToday.getDate();
//     var year = dtToday.getFullYear();
//     if (month < 0)
//         month = '0' + month.toString();
//     if (day < 10)
//         day = '0' + day.toString();
//     var minDate = year + '-' + month + '-' + day;
//     $('.txtDate').attr('max', minDate);
// })

//valid data for paste in id
var data = new Date();
var tdate = data.getDate();
var month = data.getMonth() + 1;
if (tdate < 10) {
    tdate = "0" + tdate;
}
if (month < 10) {
    month = "0" + month;
}
var year = data.getUTCFullYear();
var minDate = year + "-" + month + "-" + tdate;
document.getElementById("date_picker1").setAttribute("min", minDate);





//valid data with class
// var data1 = new Date();
// var tdate1 = data1.getDate();
// var month1 = data1.getMonth() + 1;
// if (tdate1 < 10) {
//     tdate1 = "0" + tdate1;
// }
// if (month1 < 10) {
//     month1 = "0" + month1;
// }
// var year1 = data1.getUTCFullYear();
// var minDate1 = year1 + "-" + month1 + "-" + tdate1;
// document.getElementById("date_picker1").setAttribute("min", minDate1);



//modal popaup send data
const wrapper = document.querySelector('.wrapper'),
    form = wrapper.querySelectorAll('.form'),
    submitInput = form[0].querySelector('input[type="submit"]');

function getDataForm(e) {
    e.preventDefault();

    var formData = new FormData(form[0]);

    alert(formData.get('apartaments') + ' - ' + formData.get('checkinField') + ' - ' + formData.get('checkoutField') + ' - ' + formData.get('adults')
        + ' - ' + formData.get('children') + ' - ' + formData.get('rooms') + ' - ' + formData.get('email') +
        ' - ' + formData.get('cel'));
}

document.addEventListener('DOMContentLoaded', function () {
    submitInput.addEventListener('click', getDataForm, false);
}, false)
